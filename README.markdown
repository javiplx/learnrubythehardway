# Learn Ruby The Hard Way

A [Jekyll](http://jekyllrb.com) site for [Learn Ruby The Hard Way](http://learncodethehardway.org/ruby/).

This is a translation of the original work [Learn Python The Hard Way](http://learnpythonthardway.org/) by Zed A. Shaw.
