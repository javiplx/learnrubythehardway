---
layout: post
title: "Exercise 40: Dictionaries, Oh Lovely Dictionaries"
---
# Exercise 40: Dictionaries, Oh Lovely Dictionaries
Now I have to hurt you with another container you can use, because once you learn this container a massive world of ultra-cool will be yours. It is the most useful container ever: the hash.

Ruby calls them "hashes", other languages call them, "dictionaries". I tend to use both names, but it doesn't matter. What does matter is what they do when compared to arrays. You see, an array lets you do this:

{% highlight ruby linenos %}
    ruby-1.9.2-p180 :015 > things = ['a','b','c','d']
     => ["a", "b", "c", "d"] 
    ruby-1.9.2-p180 :016 > print things[1]
    b => nil 
    ruby-1.9.2-p180 :017 > things[1] = 'z'
     => "z" 
    ruby-1.9.2-p180 :018 > print things[1]
    z => nil 
    ruby-1.9.2-p180 :019 > print things
    ["a", "z", "c", "d"] => nil 
    ruby-1.9.2-p180 :020 >
{% endhighlight %}

You can use numbers to "index" into an array, meaning you can use numbers to find out what's in arrays. You should know this by now, but what a hash does is let you use *anything*, not just numbers. Yes, a hash associates one thing to another, no matter what it is. Take a look:

{% highlight ruby linenos %}
    ruby-1.9.2-p180 :001 > stuff = {:name => "Rob", :age => 30, :height => 5*12+10}
     => {:name=>"Rob", :age=>30, :height=>70} 
    ruby-1.9.2-p180 :002 > puts stuff[:name]
    Rob
     => nil 
    ruby-1.9.2-p180 :003 > puts stuff[:age]
    30
     => nil 
    ruby-1.9.2-p180 :004 > puts stuff[:height]
    70
     => nil 
    ruby-1.9.2-p180 :005 > stuff[:city] = "New York"
     => "New York" 
    ruby-1.9.2-p180 :006 > puts stuff[:city]
    New York
     => nil 
    ruby-1.9.2-p180 :007 > 
{% endhighlight %}

You will see that instead of just numbers we're using symbols, which are just lightweight strings (think name tags) in Ruby, to say what we want from the `stuff` hash. We can also put new things into the hash with symbols. It doesn't have to be symbols though, we can also do this:

{% highlight ruby linenos %}
    ruby-1.9.2-p180 :004 > stuff[1] = "Wow"
     => "Wow" 
    ruby-1.9.2-p180 :005 > stuff[2] = "Neato"
     => "Neato" 
    ruby-1.9.2-p180 :006 > puts stuff[1]
    Wow
     => nil 
    ruby-1.9.2-p180 :007 > puts stuff[2]
    Neato
     => nil 
    ruby-1.9.2-p180 :008 > puts stuff
    {:name=>"Rob", :age=>30, :height=>70, :city=>"New York", 1=>"Wow", 2=>"Neato"}
     => nil 
    ruby-1.9.2-p180 :009 >
{% endhighlight %}

In this one I just used numbers. I could use anything. Well almost but just pretend you can use anything for now.

Of course, a hash that you can only put things in is pretty stupid, so here's how you delete things, with the `delete` function:

{% highlight ruby linenos %}
    ruby-1.9.2-p180 :009 > stuff.delete(:city)
     => "New York" 
    ruby-1.9.2-p180 :010 > stuff.delete(1)
     => "Wow" 
    ruby-1.9.2-p180 :011 > stuff.delete(2)
     => "Neato" 
    ruby-1.9.2-p180 :012 > stuff
     => {:name=>"Rob", :age=>30, :height=>70} 
    ruby-1.9.2-p180 :013 > 
{% endhighlight %}

We'll now do an exercise that you must study *very* carefully. I want you to type this exercise in and try to understand what's going on. It is a very interesting exercise that will hopefully make a big light turn on in your head very soon.

{% highlight ruby linenos %}
    cities = {'CA' => 'San Francisco', 
      'MI' => 'Detroit',
      'FL' => 'Jacksonville'}
    
    cities['NY'] = 'New York'
    cities['OR'] = 'Portland'
    
    def find_city(map, state)
      if map.include? state
        return map[state]
      else
        return "Not found."
      end
    end
    
    # ok pay attention!
    cities[:find] = method(:find_city)
    
    while true
      print "State? (ENTER to quit) "
      state = gets.chomp
    
      break if state.empty?
    
      # this line is the most important ever! study!
      puts cities[:find].call(cities, state)
    end
{% endhighlight %}

## What You Should See

    $ ruby ex40.rb 
    State? (ENTER to quit) > CA
    San Francisco
    State? (ENTER to quit) > FL
    Jacksonville
    State? (ENTER to quit) > O
    Not found.
    State? (ENTER to quit) > OR
    Portland
    State? (ENTER to quit) > VT
    Not found.
    State? (ENTER to quit) >

## Extra Credit
1. Go find the Ruby documentation for hashes and try to do even more things to them.
2. Find out what you can't do with hashes. A big one is that they do not have order, so try playing with that.
3. Try doing a `for-loop` over them, and then try the `each` method of iterating through a hash.
